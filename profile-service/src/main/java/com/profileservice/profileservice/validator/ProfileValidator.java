package com.profileservice.profileservice.validator;

import com.profileservice.profileservice.interfaces.IValidator;
import com.profileservice.profileservice.server.domain.Profile;
import org.springframework.stereotype.Component;

@Component
public class ProfileValidator implements IValidator {


    @Override
    public void validationProfile(Profile profile) {

    }

    @Override
    public boolean validProfile(Profile user) {
        return true;
    }

    @Override
    public boolean validCreateProfile(Profile user) {
        return true;
    }

}
