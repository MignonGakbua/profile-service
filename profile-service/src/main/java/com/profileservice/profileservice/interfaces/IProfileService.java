package com.profileservice.profileservice.interfaces;

import com.profileservice.profileservice.server.domain.Profile;

import java.util.List;


public interface IProfileService extends IGenericService<Profile> {

    Profile findProfileByUsername(String username);

    @Override
    Profile create(Profile profile);

    @Override
    List<Profile> read();

    @Override
    Profile update(Profile profile);

    @Override
    boolean delete(Profile profile);
}
