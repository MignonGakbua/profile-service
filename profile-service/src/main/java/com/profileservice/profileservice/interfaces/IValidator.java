package com.profileservice.profileservice.interfaces;

import com.profileservice.profileservice.server.domain.Profile;


public interface IValidator  {

    void validationProfile (Profile profile);
    boolean validProfile(Profile user);
    boolean validCreateProfile(Profile user);
}
