package com.profileservice.profileservice.server.domain;

import javax.annotation.Resource;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "profiles")
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Column(nullable = false)
    @NotNull
    private String profileTitle;
    @Column(nullable = false , unique = true)
    @NotNull
    private String username;
    private String description;
    private  String email;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] profilePicture;

    public Profile()
    {

    }


    public Long getId() {
        return Id;
    }

    public String getProfileTitle() {
        return profileTitle;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getProfilePicture() {
        return profilePicture;
    }

    public String getEmail()
    {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public void setId(Long id) {
        Id = id;
    }

    public void setProfileTitle(String profileTitle) {
        this.profileTitle = profileTitle;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setProfilePicture(byte[] profilePicture) {
        this.profilePicture = profilePicture;
    }

    public void SetUsername(String username)
    {
        this.username =username;
    }
}
