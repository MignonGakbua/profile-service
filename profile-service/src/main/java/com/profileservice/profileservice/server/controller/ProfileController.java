package com.profileservice.profileservice.server.controller;

import com.profileservice.profileservice.helpers.ErrorCode;
import com.profileservice.profileservice.helpers.JsonLogic;
import com.profileservice.profileservice.helpers.JsonResult;
import com.profileservice.profileservice.server.domain.Profile;
import com.profileservice.profileservice.service.ProfileService;
import com.profileservice.profileservice.validator.ProfileValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.profileservice.profileservice.helpers.Logger.log;

@RequestMapping("/profile")
@RestController
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @Autowired
    private ProfileValidator validator;

    @GetMapping(value = "{username}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonResult> getProfileForTheUser(@PathVariable String username) {
        log(getClass(), "Get Profile by username....");
        JsonResult result = new JsonResult();
        try {
            Profile profile = profileService.findProfileByUsername(username);
            if (profile != null) {
                result.setResult(true);
                result.setItem(profile);
                result.setMessage("Profile is successfully retrieved");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                result.setResult(false);
                result.setMessage("Failed to get a profile.");
                result.setErrorCode(ErrorCode.FAILED_CREATING_PROFILE);
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            result.setResult(false);
            result.setMessage("Failed to get a profile.");
            result.setErrorCode(ErrorCode.FAILED_DELETE_PROFILE);
            result.setErrorMessage(e.toString());
            log(this.getClass(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/create-profile", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonResult> createProfile(@RequestBody String json) {
        log(getClass(), "Create new Profile....");
        JsonResult result = new JsonResult();
        try {
            Profile profile = (Profile) JsonLogic.getObject(Profile.class, json);
            if (validator.validCreateProfile(profile)) {
                Profile createdProfile = profileService.create(profile);
                if (createdProfile != null) {
                    result.setResult(true);
                    result.setItem(createdProfile);
                    result.setMessage("Profile is created successfully");
                    return new ResponseEntity<>(result, HttpStatus.OK);
                } else {
                    result.setResult(false);
                    result.setMessage("Failed creating this profile.");
                    result.setErrorCode(ErrorCode.FAILED_CREATING_PROFILE);
                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                }
            }
            log(getClass(), "Failed creating this profile......");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            result.setResult(false);
            result.setMessage("Failed creating this profile.");
            result.setErrorCode(ErrorCode.FAILED_CREATING_PROFILE);
            result.setErrorMessage(e.toString());
            log(this.getClass(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping(value = "/delete-profile", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonResult> deleteProfile(@RequestBody String json) {

        log(getClass(), "Delete the Profile....");
        JsonResult result = new JsonResult();
        try {
            Profile profile = (Profile) JsonLogic.getObject(Profile.class, json);
            if (validator.validCreateProfile(profile)) {
                boolean IsDeleted = profileService.delete(profile);
                if (IsDeleted) {
                    result.setResult(true);
                    result.setMessage("Profile is successfully deleted");
                    return new ResponseEntity<>(result, HttpStatus.OK);
                } else {
                    result.setResult(false);
                    result.setMessage("Failed deleting this profile.");
                    result.setErrorCode(ErrorCode.FAILED_DELETE_PROFILE);
                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                }
            }
            log(getClass(), "Failed deleting this profile......");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            result.setResult(false);
            result.setMessage("Failed deleting this profile.");
            result.setErrorCode(ErrorCode.FAILED_DELETE_PROFILE);
            result.setErrorMessage(e.toString());
            log(this.getClass(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/update-profile", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonResult> updateProfile(@RequestBody String json) {

        log(getClass(), "Update the Profile....");
        JsonResult result = new JsonResult();
        try {
            Profile profile = (Profile) JsonLogic.getObject(Profile.class, json);
            if (validator.validCreateProfile(profile)) {
                Profile updatedProfile = profileService.update(profile);
                if (updatedProfile != null) {
                    result.setResult(true);
                    result.setItem(updatedProfile);
                    result.setMessage("Profile is successfully updated");
                    return new ResponseEntity<>(result, HttpStatus.OK);
                } else {
                    result.setResult(false);
                    result.setMessage("Failed update this profile.");
                    result.setErrorCode(ErrorCode.FAILED_UPDATE_PROFILE);
                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                }
            }
            log(getClass(), "Failed update this profile......");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            result.setResult(false);
            result.setMessage("Failed update this profile.");
            result.setErrorCode(ErrorCode.FAILED_UPDATE_PROFILE);
            result.setErrorMessage(e.toString());
            log(this.getClass(), e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }


}
