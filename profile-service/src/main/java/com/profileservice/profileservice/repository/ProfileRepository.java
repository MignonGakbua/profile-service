package com.profileservice.profileservice.repository;

import com.profileservice.profileservice.server.domain.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;

@Repository
public interface ProfileRepository extends JpaRepository<Profile,Long>  {
    Profile findProfileByUsername(String username);

    @Modifying
    @Query(value = " update profiles p set p.profileTitle= ?1, p.description = ?2 ,p.email = ?3, p.profilePicture =?4 where p.id = ?5")
    void setUserInfoByProfile(@NotNull String profileTitle, String description, String email, byte[] profilePicture,Long id);
}
