package com.profileservice.profileservice.service;

import com.profileservice.profileservice.helpers.Logger;
import com.profileservice.profileservice.interfaces.IProfileService;
import com.profileservice.profileservice.repository.ProfileRepository;
import com.profileservice.profileservice.server.domain.Profile;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.profileservice.profileservice.helpers.Logger.log;


@Service
@Transactional
public class ProfileService implements IProfileService {

    @Autowired
    private ProfileRepository repository;

    @Override
    public Profile findProfileByUsername(String username) {

        try {
            Profile getProfile = repository.findProfileByUsername(username);
            return getProfile;
        } catch (HibernateException e) {
            Logger.log(this.getClass(), "Error while getting the profile by" + username);
            Logger.log(this.getClass(), true, e);
            throw e;
        }
    }

    @Override
    public Profile create(Profile profile) {
        try {

            Profile createdProfile = repository.save(profile);
            return createdProfile;
        } catch (HibernateException e) {
            Logger.log(this.getClass(), "Error while creating the profile" + profile.getId());
            Logger.log(this.getClass(), true, e);
            throw e;
        }
    }

    @Override
    public List<Profile> read() {
        try {
            return repository.findAll();
        } catch (HibernateException e) {
            Logger.log(this.getClass(), "Error while getting the profiles");
            Logger.log(this.getClass(), true, e);
            throw e;
        }
    }

    @Override
    public Profile update(Profile profile) {
        try {
            repository.setUserInfoByProfile(profile.getProfileTitle(),profile.getDescription(),profile.getEmail(),profile.getProfilePicture(), profile.getId());
            return findProfileByUsername(profile.getUsername());
        } catch (HibernateException e) {
            Logger.log(this.getClass(), "Error while getting the profile" + profile.getId());
            Logger.log(this.getClass(), true, e);
            throw e;
        }
    }

    @Override
    public boolean delete(Profile profile) {
        try {
            repository.delete(profile);
            return checkIfProfileExist(profile);
        } catch (HibernateException e) {
            Logger.log(this.getClass(), "Error while getting the profile" + profile.getId());
            Logger.log(this.getClass(), true, e);
            throw e;
        }
    }

    private boolean checkIfProfileExist(Profile profile) {
        return repository.existsById(profile.getId());
    }
}
